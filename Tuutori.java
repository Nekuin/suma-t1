package factorymethod;

public class Tuutori extends AterioivaOtus{

	@Override
	public Juoma createJuoma() {
		return new Vesi();
	}

}
